package com.jarsilio.android.common.menu

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.view.Menu
import android.widget.Toast
import com.jarsilio.android.common.R
import com.jarsilio.android.common.cookies.FortuneCookiesActivity
import com.jarsilio.android.common.dialog.Dialogs
import com.jarsilio.android.common.impressum.ImpressumActivity

const val IMPRESSUM_MENU_ITEM_ID = 10000
const val SEND_DEBUG_LOGS_MENU_ITEM_ID = 11000
const val PURCHASES_MENU_ITEM_ID = 12000
const val FAQ_MENU_ITEM_ID = 13000

class CommonMenu(private val activity: Activity) {
    fun addImpressumToMenu(menu: Menu) {
        menu.add(Menu.NONE, IMPRESSUM_MENU_ITEM_ID, Menu.NONE, R.string.impressum_activity_title)
        menu.findItem(IMPRESSUM_MENU_ITEM_ID).setOnMenuItemClickListener {
            val impressumActivityIntent =
                Intent(activity, ImpressumActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                }
            activity.startActivity(impressumActivityIntent)
            true
        }
    }

    fun addFaqToMenu(
        menu: Menu,
        faqUrl: String,
    ) {
        menu.add(Menu.NONE, FAQ_MENU_ITEM_ID, Menu.NONE, R.string.faq_online)
        menu.findItem(FAQ_MENU_ITEM_ID).setOnMenuItemClickListener {
            val uri = Uri.parse(faqUrl)
            try {
                activity.startActivity(Intent(Intent.ACTION_VIEW, uri))
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(activity, R.string.error_opening_links, Toast.LENGTH_SHORT).show()
            }
            true
        }
    }

    fun addSendDebugLogsToMenu(
        menu: Menu,
        faqUrl: String? = null,
    ) {
        menu.add(Menu.NONE, SEND_DEBUG_LOGS_MENU_ITEM_ID, Menu.NONE, R.string.send_debug_logs_menu_item)
        menu.findItem(SEND_DEBUG_LOGS_MENU_ITEM_ID).setOnMenuItemClickListener {
            if (faqUrl != null) {
                Dialogs(activity).showFaqDialog(faqUrl) // This method will call showReportIssueDialog if necessary
            } else {
                Dialogs(activity).showReportIssueDialog()
            }
            true
        }
    }

    fun addCookiesToMenu(menu: Menu) {
        menu.add(Menu.NONE, PURCHASES_MENU_ITEM_ID, Menu.NONE, R.string.title_activity_fortune_cookies)
        menu.findItem(PURCHASES_MENU_ITEM_ID).setOnMenuItemClickListener {
            val purchasesActivity =
                Intent(activity, FortuneCookiesActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                }
            activity.startActivity(purchasesActivity)
            true
        }
    }
}
