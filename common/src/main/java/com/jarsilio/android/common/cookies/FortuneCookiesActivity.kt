package com.jarsilio.android.common.cookies

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.jarsilio.android.common.R
import com.jarsilio.android.common.ToolbarActivity
import com.jarsilio.android.common.extensions.appName
import com.jarsilio.android.common.extensions.preferences
import java.lang.StringBuilder
import java.security.SecureRandom

/**
 * This Activity adds some nice content in the form of fortune cookies from the linux 'fortune'
 * program. This 'pro' pro version offers some new content, which the 'lite' version does not have
 * and should hopefully not violate the Google Play Store's policy due to 'duplicative content'.
 *
 * The other 'donation' WaveUp flavors (✌️, 🍻, ❤, and 💵) were violating the Google Play policy due
 * to a 'Violation of Spam policy' due to 'duplicative content'.
 */
class FortuneCookiesActivity : ToolbarActivity() {
    override val childContentLayout: Int = R.layout.content_fortune_cookies

    private val fortuneCookiesText: TextView by lazy { findViewById(R.id.fortune_cookies_text) }
    private val openCookieButton: ImageButton by lazy { findViewById(R.id.open_fortune_cookie_button) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Not calling setContentView(R.layout.bla) from here. Toolbar activity already does that.

        fortuneCookiesText.text = fromHtml(getString(R.string.fortune_cookies_text, appName))
        fortuneCookiesText.movementMethod = LinkMovementMethod.getInstance()

        openCookieButton.setOnClickListener { showCookie() }
    }

    /**
     * Default number of cookies that will be shown to the user if she doesn't buy any more.
     * As standard value of [android.content.SharedPreferences]
     */
    private val defaultUnlockedCookies = 5

    /**
     * Keeps track of the current cookie that should be shown to the user when she taps on the
     * *show a cookie* button ([openCookieButton]).
     */
    private var currentCookieIndex = -1

    /**
     * A list of random cookie indices. Instead of randomizing the list of cookies itself, we
     * create a list of random indices to access the (still) ordered list. Used by [nextCookie]
     * @see [nextCookie]
     * @see [unlockedCookies]
     */
    private val randomCookieIndices: List<Int>
        get() {
            val randomCookieIndicesId = "pref_random_cookie_indices_id"
            if (preferences.getString(randomCookieIndicesId, null) == null) {
                preferences.edit().putString(randomCookieIndicesId, commaSeparatedRandomIndices).apply()
            }

            val savedCommaSeparatedRandomIndices = preferences.getString(randomCookieIndicesId, null)

            return savedCommaSeparatedRandomIndices?.split(",")?.map { it.toInt() } ?: ArrayList()
        }

    /**
     * Maximum amount of cookies to be shown to the user while calling [nextCookie].
     */
    private var unlockedCookies: Int
        get() = preferences.getInt("pref_unlocked_cookies", defaultUnlockedCookies)
        set(value) = preferences.edit().putInt("pref_unlocked_cookies", value).apply()

    /**
     * Show a cookie using [AlertDialog].
     * @see nextCookie
     * @see randomCookieIndices
     * @see unlockedCookies
     */
    private fun showCookie() {
        AlertDialog.Builder(this).apply {
            setMessage(nextCookie())
            setPositiveButton(android.R.string.ok) { _, _ -> }
            show()
        }
    }

    /**
     * @return The next cookie until the maximum [unlockedCookies]
     */
    private fun nextCookie(): String {
        currentCookieIndex++
        if (currentCookieIndex !in 0 until unlockedCookies) {
            currentCookieIndex = 0
        }

        return FORTUNE_COOKIES[randomCookieIndices[currentCookieIndex]]
    }

    /**
     * Comma-separated list of random indices converted from [randomIndices]. Used to store the
     * random indices in the [android.content.SharedPreferences].
     */
    private val commaSeparatedRandomIndices: String by lazy {
        val stringBuilder = StringBuilder()

        for (randomIndex in randomIndices) {
            stringBuilder.append(
                if (randomIndex == randomIndices.first()) {
                    "$randomIndex"
                } else {
                    ",$randomIndex"
                },
            )
        }

        stringBuilder.toString()
    }

    /**
     * Random indices of the complete list of [FORTUNE_COOKIES].
     */
    private val randomIndices: Set<Int> by lazy {
        FORTUNE_COOKIES.indices.shuffled(SecureRandom()).toSet()
    }
}
