package com.jarsilio.android.common.prefs

import android.content.Context
import com.jarsilio.android.common.extensions.getPackageInfoCompat
import com.jarsilio.android.common.extensions.isMiui
import com.jarsilio.android.common.extensions.isPieOrNewer
import com.jarsilio.android.common.extensions.miuiVersion
import com.jarsilio.android.common.extensions.preferences
import com.jarsilio.android.common.utils.SingletonHolder

class Values private constructor(context: Context) {
    private val applicationContext: Context = context.applicationContext
    private val preferences = context.preferences

    var lastLoveDialogShownTimestamp: Long
        get() =
            preferences.getLong(
                LAST_LOVE_DIALOG_SHOWN,
                applicationContext.packageManager.getPackageInfoCompat(applicationContext.packageName, 0).firstInstallTime,
            )
        set(lastTimeLoveDialogShown) = setPreference(LAST_LOVE_DIALOG_SHOWN, lastTimeLoveDialogShown)

    var showLoveDialog: Boolean
        get() = preferences.getBoolean(SHOW_LOVE_DIALOG, true)
        set(showLoveDialogShown) = setPreference(SHOW_LOVE_DIALOG, showLoveDialogShown)

    var showGratitudeDialog: Boolean
        get() = preferences.getBoolean(SHOW_GRATITUDE_DIALOG, true)
        set(showGratitudeDialog) = setPreference(SHOW_GRATITUDE_DIALOG, showGratitudeDialog)

    var showMiuiPermissionsDialog: Boolean
        get() =
            if (miuiVersion >= 11 || isMiui && isPieOrNewer) {
                preferences.getBoolean(SHOW_MIUI_PERMISSIONS_DIALOG, true)
            } else {
                false
            }

        set(value) = setPreference(SHOW_MIUI_PERMISSIONS_DIALOG, value)

    private fun setPreference(
        key: String,
        value: Boolean,
    ) {
        // This doesn't change the GUI (which I shouldn't really care about. I am just using this class to store values)
        preferences.edit().putBoolean(key, value).apply()
    }

    private fun setPreference(
        key: String,
        value: Long,
    ) {
        preferences.edit().putLong(key, value).apply()
    }

    fun setPreference(
        key: String,
        value: List<Int>,
    ) {
        preferences.edit().putStringSet(key, value.map { it.toString() }.toHashSet()).apply()
    }

    companion object : SingletonHolder<Values, Context>(::Values) {
        const val SHOW_LOVE_DIALOG = "pref_show_love_dialog"
        const val SHOW_GRATITUDE_DIALOG = "pref_show_gratitude_dialog"
        const val LAST_LOVE_DIALOG_SHOWN = "pref_last_show_love_dialog_shown"
        const val SHOW_MIUI_PERMISSIONS_DIALOG = "pref_show_miui_permissions_dialog"
    }
}
