package com.jarsilio.android.common.extensions

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.annotation.ChecksSdkIntAtLeast
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.content.pm.PackageInfoCompat
import androidx.preference.PreferenceManager
import okio.buffer
import okio.source
import timber.log.Timber
import java.io.File
import java.io.IOException

val Context.preferences: SharedPreferences
    get() = PreferenceManager.getDefaultSharedPreferences(this)

val Context.actualPackageName: String?
    get() = javaClass.`package`?.name

val Context.applicationId: String?
    get() = packageName

val Context.flavor: String?
    get() = getBuildConfigValue(actualPackageName, "FLAVOR") as String?

val Context.appName: String
    get() {
        val applicationInfo = applicationInfo
        val stringId = applicationInfo.labelRes
        return if (stringId == 0) {
            applicationInfo.nonLocalizedLabel.toString()
        } else {
            getString(stringId)
        }
    }

val Context.appVersionName: String
    get() {
        return try {
            packageManager.getPackageInfoCompat(packageName, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            Timber.e(e, "Failed to read version name")
            "version unknown"
        }
    }

val Context.appVersionCode: Long
    get() = PackageInfoCompat.getLongVersionCode(packageManager.getPackageInfoCompat(packageName, 0))

val Context.shortAppName: String?
    get() = actualPackageName?.substringAfterLast('.')

val Context.fileProviderAuthority: String
    get() = "$applicationId.common.fileprovider"

val Context.defaultEmail: String
    get() = "juam+$shortAppName@posteo.net"

// Logging
val Context.logsDir: File
    get() = File(cacheDir.toString(), "logs") // like in filepaths.xml

val Context.mergedLogFile: File
    get() = File(logsDir, "$shortAppName.log")
val Context.mergedLogFileUri: Uri
    get() = FileProvider.getUriForFile(this, fileProviderAuthority, mergedLogFile)

val Context.logFiles: Array<File>
    get() =
        arrayOf(
            File(logsDir, "$shortAppName.log.1"),
            File(logsDir, "$shortAppName.log.2"),
        )
val Context.logFile: File
    get() = logFiles[0]
val Context.rotatedLogFile: File
    get() = logFiles[1]

fun Context.isIntentAvailable(intent: Intent): Boolean {
    return packageManager.queryIntentActivitiesCompat(intent, PackageManager.MATCH_DEFAULT_ONLY).size > 0
}

/**
 * Gets a field from the project's BuildConfig. This is useful when, for example, flavors
 * are used at the project level to set custom fields.
 * @param fieldName The name of the field-to-access
 * @return The value of the field, or `null` if the field is not found.
 */
private fun getBuildConfigValue(
    packageName: String?,
    fieldName: String,
): Any? {
    val buildConfigClassName = "$packageName.BuildConfig"
    try {
        val clazz = Class.forName(buildConfigClassName)
        val field = clazz.getField(fieldName)
        return field.get(null)
    } catch (e: ClassNotFoundException) {
        Timber.e(e, "Failed to get $buildConfigClassName.$fieldName")
    } catch (e: NoSuchFieldException) {
        Timber.e(e, "Failed to get $buildConfigClassName.$fieldName")
    } catch (e: IllegalAccessException) {
        Timber.e(e, "Failed to get $buildConfigClassName.$fieldName")
    }

    return null
}

@get:ChecksSdkIntAtLeast(api = Build.VERSION_CODES.JELLY_BEAN)
val isJellyBeanOrNewer: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN

@get:ChecksSdkIntAtLeast(api = Build.VERSION_CODES.LOLLIPOP)
val isLollipopOrNewer: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP

@get:ChecksSdkIntAtLeast(api = Build.VERSION_CODES.M)
val isMarshmallowOrNewer: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M

@get:ChecksSdkIntAtLeast(api = Build.VERSION_CODES.N)
val isNougatOrNewer: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N

@get:ChecksSdkIntAtLeast(api = Build.VERSION_CODES.O)
val isOreoOrNewer: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O

@get:ChecksSdkIntAtLeast(api = Build.VERSION_CODES.P)
val isPieOrNewer: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.P

@get:ChecksSdkIntAtLeast(api = Build.VERSION_CODES.Q)
val isQOrNewer: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q

@get:ChecksSdkIntAtLeast(api = Build.VERSION_CODES.R)
val isSOrNewer: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.S

@get:ChecksSdkIntAtLeast(api = Build.VERSION_CODES.R)
val isROrNewer: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.R

@get:ChecksSdkIntAtLeast(api = Build.VERSION_CODES.TIRAMISU)
val isTiramisuOrNewer: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU

@get:ChecksSdkIntAtLeast(api = Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
val isUpsideDownCakeOrNewer: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE

val isMiui: Boolean
    get() = getSystemProperty("ro.miui.ui.version.name")?.isNotBlank() == true

val miuiVersion: Int
    get() {
        var version = -1

        val versionName = getSystemProperty("ro.miui.ui.version.name")
        if (versionName != null) {
            try {
                version = versionName.substring(1).toInt()
            } catch (e: Exception) {
                Timber.e(e, "Failed to transform miui.version.name to integer. Version name : $versionName")
            }
        } else {
            Timber.w("Failed to read ro.miui.ui.version.name to determine MIUI version.")
        }

        return version
    }

val Context.isPostNotificationsPermissionGranted: Boolean
    get() {
        val permissionCheck =
            if (isTiramisuOrNewer) {
                ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.POST_NOTIFICATIONS)
            } else {
                PackageManager.PERMISSION_GRANTED
            }
        return permissionCheck == PackageManager.PERMISSION_GRANTED
    }

fun getSystemProperty(propertyName: String): String? {
    val command = "getprop $propertyName"
    Timber.d("Running '$command'")
    return try {
        Runtime.getRuntime().exec(command).inputStream.source().buffer().use { source ->
            val line = source.readUtf8Line()
            Timber.d("Property value: '$line'")
            line
        }
    } catch (e: IOException) {
        Timber.e("Failed to run '$command'")
        null
    }
}
