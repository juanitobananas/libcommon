/*
 * Copyright (c) 2018 Juan García Basilio
 *
 * This file is part of privacy-policy-lib.
 *
 * privacy-policy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * privacy-policy-lib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with privacy-policy-lib.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.common.privacypolicy;

import com.jarsilio.android.common.R;

public class PrivacyPolicyActivity extends SimpleHtmlAbstractActivity<PrivacyPolicyBuilder> {

    @Override
    String getActivityTitle() {
        String title = getBuilder().getActivityTitle();
        if (title == null) {
            title = getString(R.string.title_activity_privacy_policy);
        }
        return title;
    }

    @Override
    String getHtmlContent() {
        StringBuilder stringBuilder = new StringBuilder();

        boolean introSection = getBuilder().isIntroSection();
        String appName = getBuilder().getAppName();
        String author = getBuilder().getAuthor();
        String url = getBuilder().getUrl();

        if (introSection) {
            if (author != null) {
                stringBuilder.append(getString(R.string.intro_with_author, appName, author));
            } else {
                stringBuilder.append(getString(R.string.intro, appName));
            }

            if (url != null) {
                stringBuilder.append(getString(R.string.outdated_text_and_link_to_online_version, url));
            }
        }

        boolean meSection = getBuilder().isMeSection();
        if (meSection) {
            stringBuilder.append(getString(R.string.me_section));
        }

        boolean autoGoogleOrFDroidSection = getBuilder().isAutoGoogleOrFDroidSection();
        if (autoGoogleOrFDroidSection) {
            String installer = getPackageManager().getInstallerPackageName(getPackageName());
            if (installer != null && installer.equals("com.android.vending")) {
                stringBuilder.append(getString(R.string.google_section));
            } else if (installer != null && installer.startsWith("org.fdroid.fdroid")) { // It could be org.fdroid.fdroid.privileged too e.g.
                stringBuilder.append(getString(R.string.f_droid_section));
            }
        } else {
            boolean googlePlaySection = getBuilder().isGooglePlaySection();
            if (googlePlaySection) {
                stringBuilder.append(getString(R.string.google_section));
            }

            boolean fDroidSection = getBuilder().isfDroidSection();
            if (fDroidSection) {
                stringBuilder.append(getString(R.string.f_droid_section));
            }
        }

        String email = getBuilder().getEmail();
        if (email != null) {
            stringBuilder.append(getString(R.string.email_section, email, email));
        }

        stringBuilder.append(getString(R.string.acra_section, email, email));

        String customSection = getBuilder().getCustomSection();
        if (customSection != null) {
            stringBuilder.append(customSection);
        }

        return stringBuilder.toString();
    }
}
