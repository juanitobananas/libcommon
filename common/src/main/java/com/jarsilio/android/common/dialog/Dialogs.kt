package com.jarsilio.android.common.dialog

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.InputType
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.jarsilio.android.common.R
import com.jarsilio.android.common.extensions.appName
import com.jarsilio.android.common.extensions.flavor
import com.jarsilio.android.common.extensions.isPostNotificationsPermissionGranted
import com.jarsilio.android.common.logging.LogSender
import com.jarsilio.android.common.prefs.Values
import timber.log.Timber

@Suppress("unused")
class Dialogs(private val context: Context) {
    private val values = Values.getInstance(context)

    fun showSoLongAndThanksForAllTheFishDialog(flavor: String? = context.flavor) {
        when (flavor) {
            "peace", "cheers", "love", "dollar" -> {
                AlertDialog.Builder(context).apply {
                    setTitle(R.string.discontinued_title)
                    setMessage(R.string.discontinued_explanation)
                    setPositiveButton(android.R.string.ok) { _, _ -> }
                    show()
                }
            }
        }
    }

    fun showSomeLoveDialogIfNecessary(flavor: String? = context.flavor) {
        if (flavor == "standard" || flavor == "googleSucks" || flavor == null) {
            if (flavor == null) {
                Timber.e(
                    "Couldn't read the flavor from main app's BuildConfig. " +
                        "Assuming it is 'standard' (we won't thank people that have bought the app like this)",
                )
            }
            val now = System.currentTimeMillis()
            if (values.showLoveDialog && now - values.lastLoveDialogShownTimestamp > TWO_WEEKS) {
                values.lastLoveDialogShownTimestamp = now
                AlertDialog.Builder(context).apply {
                    setTitle(context.getString(R.string.show_some_love_dialog_title, context.appName))
                    setMessage(context.getString(R.string.show_some_love_dialog_text, context.appName))
                    setPositiveButton(R.string.take_me_there) { _, _ -> goToGooglePlayStore() }
                    setNeutralButton(R.string.maybe_later) { _, _ -> }
                    setNegativeButton(R.string.never_again) { _, _ -> values.showLoveDialog = false }
                    setCancelable(false)
                    show()
                }
            }
        } else {
            if (values.showGratitudeDialog) {
                AlertDialog.Builder(context).apply {
                    setTitle(R.string.show_gratitude_dialog_title)
                    setMessage(context.getString(R.string.show_gratitude_dialog_text, context.appName))
                    setPositiveButton(android.R.string.ok) { _, _ -> values.showGratitudeDialog = false }
                    show()
                }
            }
        }
    }

    private fun goToGooglePlayStore() {
        val marketSearchUri = Uri.parse("market://search?q=pub:juanitobananas")
        val goToMarket = Intent(Intent.ACTION_VIEW, marketSearchUri)
        goToMarket.addFlags(
            Intent.FLAG_ACTIVITY_NO_HISTORY or
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK,
        )
        try {
            context.startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            val marketDevUri = Uri.parse("http://play.google.com/store/apps/developer?id=juanitobananas")
            try {
                context.startActivity(Intent(Intent.ACTION_VIEW, marketDevUri))
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(context, R.string.error_opening_links, Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun showFaqDialog(faqUrl: String) {
        AlertDialog.Builder(context).apply {
            setTitle(R.string.already_read_faq)
            setMessage(R.string.read_faq_before_reporting_issue)
            setPositiveButton(R.string.read_faq) { _, _ ->
                val uri = Uri.parse(faqUrl)
                try {
                    context.startActivity(Intent(Intent.ACTION_VIEW, uri))
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(context, R.string.error_opening_links, Toast.LENGTH_SHORT).show()
                }
            }
            setNegativeButton(R.string.report_issue_anyway) { _, _ ->
                showReportIssueDialog()
            }
            show()
        }
    }

    fun showReportIssueDialog() {
        val params =
            FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
                leftMargin = context.resources.getDimensionPixelSize(R.dimen.dialog_margin)
                rightMargin = context.resources.getDimensionPixelSize(R.dimen.dialog_margin)
            }
        val input =
            EditText(context).apply {
                inputType = InputType.TYPE_CLASS_TEXT or
                    InputType.TYPE_TEXT_FLAG_MULTI_LINE or
                    InputType.TYPE_TEXT_FLAG_CAP_SENTENCES or
                    InputType.TYPE_TEXT_FLAG_AUTO_CORRECT
                layoutParams = params
                requestFocus()
                postDelayed(
                    {
                        val keyboard = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        keyboard.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
                    },
                    // This is just to show the keyboard directly. For some reason, if I run it immediately, it doesn't work (at least on my devices)
                    100,
                )
            }

        val frameLayout = FrameLayout(context)
        frameLayout.addView(input)
        AlertDialog.Builder(context).apply {
            setTitle(R.string.send_debug_logs_menu_item)
            setMessage(R.string.please_describe_the_issue_title)
            setPositiveButton(R.string.acra_notification_send) { _, _ -> LogSender(context, input.text.toString()).send() }
            setNegativeButton(android.R.string.cancel) { _, _ -> }
            setView(frameLayout)
            show()
        }
    }

    // The calling app needs to have POST_NOTIFICATIONS permission in its AndroidManifest.xml
    fun showPostNotificationsExplanationIfNecessary(activity: Activity) {
        if (activity.isPostNotificationsPermissionGranted) {
            Timber.d("Not opening post notifications permissions dialog (permission already granted).")
            return
        }

        Timber.d("Showing dialog to see if the user wants to request Android's 'POST_NOTIFICATIONS'")
        AlertDialog.Builder(activity).apply {
            setMessage(context.getString(R.string.post_notifications_explanation, context.appName))
            setPositiveButton(R.string.request_permission) { _, _ ->
                Timber.d("Requesting Android's 'Post Notifications' permission")
                ActivityCompat.requestPermissions(
                    activity,
                    arrayOf("android.permission.POST_NOTIFICATIONS"),
                    1001,
                )
            }
            setNegativeButton(R.string.ignore_permission) { _, _ ->
                Timber.d("User decided *not* to request 'Post Notifications' permission")
            }
            // Just so that tapping somewhere else doesn't dismiss the dialog. It can still be canceled with the button.
            setCancelable(false)
            show()
        }
    }

    companion object {
        private const val ONE_HOUR = 60 * 60 * 1000
        private const val ONE_DAY = 24 * ONE_HOUR
        private const val ONE_WEEK = 1 * 7 * ONE_DAY
        private const val TWO_WEEKS = 2 * ONE_WEEK
    }
}
