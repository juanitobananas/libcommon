/*
 * Copyright (c) 2018 Juan García Basilio
 *
 * This file is part of privacy-policy-lib.
 *
 * privacy-policy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * privacy-policy-lib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with privacy-policy-lib.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.common.privacypolicy;

import android.os.Parcel;

@SuppressWarnings("unused")
public class PrivacyPolicyBuilder extends SimpleHtmlAbstractActivityBuilder {
    private String appName = null;
    private String activityTitle = null;
    private String author = null;
    private boolean introSection = false;
    private String url = null;
    private String email = null;
    private boolean meSection = false;
    private boolean autoGoogleOrFDroidSection = false;
    private boolean fDroidSection = false;
    private boolean googlePlaySection = false;
    private String customSection = null;

    public PrivacyPolicyBuilder() {
    }

    @Override
    Class getActivityClass() {
        return PrivacyPolicyActivity.class;
    }

    public PrivacyPolicyBuilder withActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle;
        return this;
    }

    public PrivacyPolicyBuilder withIntro(String appName) {
        this.introSection = true;
        this.appName = appName;
        return this;
    }

    public PrivacyPolicyBuilder withIntro(String appName, String author) {
        this.introSection = true;
        this.appName = appName;
        this.author = author;
        return this;
    }

    public PrivacyPolicyBuilder withUrl(String url) {
        this.url = url;
        return this;
    }

    public PrivacyPolicyBuilder withMeSection() {
        this.meSection = true;
        return this;
    }

    public PrivacyPolicyBuilder withAutoGoogleOrFDroidSection() {
        this.autoGoogleOrFDroidSection = true;
        return this;
    }

    public PrivacyPolicyBuilder withFDroidSection() {
        this.fDroidSection = true;
        return this;
    }

    public PrivacyPolicyBuilder withGooglePlaySection() {
        this.googlePlaySection = true;
        return this;
    }

    public PrivacyPolicyBuilder withEmailSection(String email) {
        this.email = email;
        return this;
    }

    public PrivacyPolicyBuilder withCustomSection(String htmlText) {
        this.customSection = htmlText;
        return this;
    }

    private PrivacyPolicyBuilder(Parcel in) {
        appName = in.readString();
        activityTitle = in.readString();
        author = in.readString();
        introSection = in.readByte() != 0;
        url = in.readString();
        email = in.readString();
        meSection = in.readByte() != 0;
        autoGoogleOrFDroidSection = in.readByte() != 0;
        fDroidSection = in.readByte() != 0;
        googlePlaySection = in.readByte() != 0;
        customSection = in.readString();
    }

    public static final Creator<PrivacyPolicyBuilder> CREATOR = new Creator<PrivacyPolicyBuilder>() {
        @Override
        public PrivacyPolicyBuilder createFromParcel(Parcel in) {
            return new PrivacyPolicyBuilder(in);
        }

        @Override
        public PrivacyPolicyBuilder[] newArray(int size) {
            return new PrivacyPolicyBuilder[size];
        }
    };

    @SuppressWarnings("SameReturnValue")
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(appName);
        dest.writeString(activityTitle);
        dest.writeString(author);
        dest.writeInt(introSection ? 1 : 0);
        dest.writeString(url);
        dest.writeString(email);
        dest.writeInt(meSection ? 1 : 0);
        dest.writeInt(autoGoogleOrFDroidSection ? 1 : 0);
        dest.writeInt(fDroidSection ? 1 : 0);
        dest.writeInt(googlePlaySection ? 1 : 0);
        dest.writeString(customSection);
    }

    public String getAppName() {
        return appName;
    }

    public String getActivityTitle() {
        return activityTitle;
    }

    public String getAuthor() {
        return author;
    }

    public boolean isIntroSection() {
        return introSection;
    }

    public String getUrl() {
        return url;
    }

    public String getEmail() {
        return email;
    }

    public boolean isMeSection() {
        return meSection;
    }

    public boolean isAutoGoogleOrFDroidSection() {
        return autoGoogleOrFDroidSection;
    }

    public boolean isfDroidSection() {
        return fDroidSection;
    }

    public boolean isGooglePlaySection() {
        return googlePlaySection;
    }

    public String getCustomSection() {
        return customSection;
    }
}
