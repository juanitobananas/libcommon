package com.jarsilio.android.common

import android.os.Bundle
import android.text.Spanned
import android.view.LayoutInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.text.HtmlCompat

abstract class ToolbarActivity : AppCompatActivity() {
    abstract val childContentLayout: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_toolbar)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        // Add back button
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        val layoutInflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val parentLayout = findViewById<CoordinatorLayout>(R.id.toolbar_layout_id)
        parentLayout.addView(layoutInflater.inflate(childContentLayout, parentLayout, false))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressedDispatcher.onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun fromHtml(source: String): Spanned {
            return HtmlCompat.fromHtml(source, HtmlCompat.FROM_HTML_MODE_LEGACY)
        }
    }
}
