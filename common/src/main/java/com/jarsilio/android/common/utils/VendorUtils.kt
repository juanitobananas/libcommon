package com.jarsilio.android.common.utils

import android.content.Context
import android.content.Intent
import com.jarsilio.android.common.extensions.isIntentAvailable
import com.jarsilio.android.common.prefs.Values
import timber.log.Timber

class VendorUtils(private val context: Context) {
    private val values by lazy { Values.getInstance(context) }

    // Xiaomi / MIUI
    fun openMiuiPermissionsDialogIfNecessary() {
        if (values.showMiuiPermissionsDialog) {
            val intent =
                Intent("miui.intent.action.APP_PERM_EDITOR").apply {
                    setClassName("com.miui.securitycenter", "com.miui.permcenter.permissions.PermissionsEditorActivity")
                    putExtra("extra_pkgname", context.packageName)
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                }

            if (context.isIntentAvailable(intent)) {
                context.startActivity(intent)
            } else {
                Timber.e("Couldn't find Intent to open MIUI special settings")
            }
        }
    }
}
