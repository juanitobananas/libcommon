package com.jarsilio.android.common.privacypolicy;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jarsilio.android.common.R;
import com.jarsilio.android.common.ToolbarActivity;

import timber.log.Timber;

public abstract class SimpleHtmlAbstractActivity <T extends SimpleHtmlAbstractActivityBuilder> extends ToolbarActivity {

    private T builder;

    abstract String getActivityTitle();
    abstract String getHtmlContent();

    @Override
    public int getChildContentLayout() {
        return R.layout.content_simple_html_abstract;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setBuilder();
        setTitle(getActivityTitle());
        setContentView(R.layout.activity_simple_html_abstract);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Add back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        TextView textView = new TextView(this);
        textView.setText(Companion.fromHtml(getHtmlContent()));
        textView.setMovementMethod(LinkMovementMethod.getInstance());

        LinearLayout linearLayout = findViewById(R.id.simple_html_abstract_linear_layout);
        linearLayout.addView(textView);
    }

    private void setBuilder() {
        Intent intent = getIntent();
        builder = intent.getParcelableExtra("builder");
        if (builder == null) {
            Timber.e("Parcelable 'builder' cannot be null. You can only start PrivacyPolicyActivity using the Builder");
            throw new IllegalArgumentException("Parcelable 'parcel' cannot be null while starting PrivacyPolicyActivity. You can only start PrivacyPolicyActivity using the Builder");
        }
    }

    T getBuilder() {
        return builder;
    }
}
