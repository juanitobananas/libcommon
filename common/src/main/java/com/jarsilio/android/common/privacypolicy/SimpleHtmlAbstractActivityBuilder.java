package com.jarsilio.android.common.privacypolicy;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;

public abstract class SimpleHtmlAbstractActivityBuilder <T extends SimpleHtmlAbstractActivity> implements Parcelable {
    abstract Class<T> getActivityClass();

    public void start(Context context) {
        Intent intent = new Intent(context, getActivityClass());
        intent.putExtra("builder", this);
        // Because we are starting an Activity from a non-Activity context
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
